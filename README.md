# Initiation au Web #

Ce projet a pour but d'etre un tres simple survole des bases et concepte de la programmation web
Dans ce projet nous y verrons les base par etape, soit le HTML, le CSS et le Javascript.

### Table des matieres ###

1. Materiel necessaire
    1. [Editeur texte](https://code.visualstudio.com/)
    2. [Python](https://www.python.org/)
2. HTML
    1. Ma premiere page web
    2. Les "elements"
    3. Mon site web sur un server
3. CSS
    1. Les bases
    2. Structure et organisation
    3. Framework CSS
4. Javascript
    1. Ma premiere ligne de code
    2. Evenementiation
    3. Ma calculette
    4. Librairie Javascript

### 1. Materiel necessaire ###

Afin de creer notre site web et de le tester, vous aurez besoin de certaine application pour vous rendre la tache le plus simple que possible.

#### 1.1. Editeur texte ####

Tout code peut ce faire via n'importe quel editeur de texte... mais certains sont mieux que d'autre pour programmer. [Visual Code](https://code.visualstudio.com/) par exemple est probablement l'un des meilleurs editeur de texte specialement concus pour de la programmation en tout genre et le plus fantastique dans cette histoire est qu'il est gratuit.
Si vous le desirez, vous pouver utiliser n'importe quel autre editeur de votre choix, mais sachez que certain de mais exemple utilise des fonctionnaliters de VSCode (de son petit nom).

#### 1.2. [Python](https://www.python.org/) ####

Afin de vous rendre les choses le plus simple possible, j'ai creer deux fichiers executable windows nommes "py_server_v[x].bat". C'est dernier necessite que vous possedier Python d'installer sur votre poste et que lors de sont installation, vous ayez ajouter au "Path" de windows la reference de Python.
Sur le site, vous aurez le choix entre la version 2 ou 3. Prener la version qui vous attire le plus et installer cette derniere, nous y reviendrons un peut plus tard.

### 2. HTML ###

Le HTML est en quelque sort un langage visuel. En utilisant ce dernier, vous remarquerez que celui-ci ne fait que disposer les informations au endroit dont vous aurez choisi.
Le langage HTML passe par des "elements", nom donnee a tous ce qui ce trouve entre "<>" et qui est reconnut par les navigateurs web (IE, Chrome, Firefox, etc).

Impatient de commencer a ecrire une page web ? 

Super, cree un repertoire nommer "ma_premiere_page_web" et dans ce dernier creer un fichier nomme "index.html". Suite a cela, ouvrez le repertoire "ma_premiere_page_web" avec VSCode (File->Open Folder). Votre fichier "index.html" devrais ce retrouver dans l'explorateur.

#### 2.1. Ma premiere page web ####

Editer le fichier "index.html" pour que ce dernier ressemble a ceci :

    <html>
    <body>
        Hello world !!!
    </body>
    </html>

Faite vous plaisir et ouvrez "index.html" a l'aide d'un navigateur web... et TADA !!! votre toute premiere page web !!! Bon d'accord, elle est un peut timorer, mais ce n'est pas trop mal pour 5 lignes de code ! Allons lui ajouter un peut de tenus ;)

#### 2.2. Les elements ####

Le text que vous venez d'ecrire n'a rien de banale et d'inutile, en fait vous venez d'ecire 2 elements HTML : "<html>" et "<body>".
Le premier servant a indiquer au navigateur que ce document est une page web et le second, d'afficher le contenue de cette derniere. C'est deux elements forme le base d'une page web, mais on ne construit pas une maison qu'avec de la terre. Plusieurs autres elements existe et on chacun une fonction bien precise. Pour reference utiliser [w3schools](https://www.w3schools.com/), il y a meme des cours comme ce dernier (en plus avancer) si le coeur vous en dit.

OH, une autre chose tres importante, il faut toujours fermer nos balise d'element. Sans quoi le navigateur ne sais pas quand il doit arreter de mettre du contenu de ce dernier. (nb: certain element, comme les input, n'ont pas de seconde balise nous les ecrivont donc <input />.)

Mais treve de bavardage et passons au chose interessante :

    <html>
    <body>
        <h4>Calculette</h4>
        
        <div id="formulaHolder"></div>

        <div>
            <input type="text" id="inputNumber" disabled />
        </div>
        <div>
            <button type="button" id="btnPlus">+</button>
            <button type="button" id="btnMinus">-</button>
            <button type="button" id="btnTime">*</button>
            <button type="button" id="btnPer">/</button>
        </div>
    </body>
    </html>

Vous voyer tranquillement ou ceci va nous mener ? Ne vous en faite pas, il y a encore beaucoup a faire (mais pas trop).

Oh, je vois une main leve.

> Pourquoi est-ce que tu ecris id="trucMuch" ?

La raison est simple, ceci donne un nom a un element precis. De cette facon, le navigateur est en mesure de reconnaitre de qui l'on parle. Voici un exemple :

> Nous somme dans une maison ou vit vingts chats (nos elements [<div>, <button>, etc.])
> Tu me dit : Amene le chat chez le toiletteur
> Moi dans ma tete : (... pour etre sur d'amenez le bon je vais devoir tous les amener et bonjour la facture...)

mais avec les id :

> Toi : Amenez Flocon chez le toiletteur
> Moi : (Avec les minouches, je part attraper Flocon)

Une autre chose tres importante avec l'attribut id, ce dernier doit etre unique, en aucun temps un element doit porter le meme nom qu'un autre, sinon le navigateur web ne considaire que le premier qu'il croise (ce qui peut engendrer beaucoup de confision dans votre code javascript, j'en ferai mention plus loin)

... une autre main leve

>
> Que signifit "disabled" dans l'element "inputNumber" ?
>

Certain element un des attributs particulier, "type" est un attribut tout comme "disabled". Dans notre cas, ce dernier empeche l'utilisateur d'ecrire a l'interieur du champ text "inputNumber". Vous comprendrz bien assez tot la raison deriere ceci ;)

Ceci nous seras tres utile plus tard, pour l'instant continuont.

    [...]
        </div>
        <div>
            <button type="button" class="opperator" id="btn_Plus">+</button>
            <button type="button" class="opperator" id="btn_Minus">-</button>
            <button type="button" class="opperator" id="btn_Time">*</button>
            <button type="button" class="opperator" id="btn_Per">/</button>
        </div>
        <div>
            <button type="button" class="number">7</button>
            <button type="button" class="number">8</button>
            <button type="button" class="number">9</button>
        </div>
        <div>
            <button type="button" class="number">4</button>
            <button type="button" class="number">5</button>
            <button type="button" class="number">6</button>
        </div>
        <div>
            <button type="button" class="number">3</button>
            <button type="button" class="number">2</button>
            <button type="button" class="number">1</button>
        </div>
        <div>
            <button type="button" class="number">0</button>
        </div>
    <body>

Bon, notre calculette commence a ressembler a quelque chose, mais elle reste vraiment timorer. Nous allons remedier a la situation sous peu, mais nous devons parler d'autre chose.

#### 2.3. Mon site web sur un server ####

Nous avons afficher le contenue de notre page "index.html" dans un navigateur web, mais regarder son adresse URL vous y verrez quelque chose de singuler : le chemin du fichier sur votre disque dure. Ce qui n'est vraiment pas genial et quand est-il si notre site web contient plus d'une page ? Cette facon n'est plus viable a present. Pour remedier a cette situation je vous presente les serveurs web !

Il faut voir un serveur web comme un repertoire virtuel qui indique au navigateur quel fichier lire et dans quel ordre. De plus l'URL sera elle aussi virtuel (plus de C:///repertoire). Vous vous souvenez d'avoir installer Python ? L'une de c'est fonctionnaliter (une parmis des milliers) est de creer un serveur web pour un repertoire donne. Ouvrer le repertoire serveur du repertoire de cours et copier le fichier "py_server_v[version que vous avez installer].bat" et coller ce dernier dans votre repertoire "ma_premiere_page_web" puis, double cliquer sur ce dernier, une invite de commande s'ouvrira. Tant que cette fenetre est ouverte, votre serveur web est vivant !

Retourner a votre navigateur web et changer l'URL pour "localhost:8000". (TADAA !!!)

Nous avons donc un serveur locale pour afficher notre site web. Notez que ce votre page n'est pas accessible via d'autre ordinateur a l'exterieur de votre reseau. Votre page web n'est pas "sur le web". Afin de rendre accessible sur le web vos future page web, vous aurez besoin d'un serveur d'hebergement et d'un nom de domaine.

PS: A partir de maintenant, il ne vous suffit qu'a appuyer sur "F5" voir vos modification ou "Ctr + F5" (pour vider la cache et rafrechir la page, dans le cas de modification de fichier javascript). Debille non ?

### 3. CSS ###

L'utilite des CSS (Cascade Style Sheet) sert a colorer et styliser vos element formant votre page web. La structure de ces dernieres est un peu differente de notre page HTML.

Allons changer un peu la gueule de notre calculette

#### 3.1. Les bases ####

Dans votre repertoire "ma_premiere_page_web", cree un nouveau fichier nomme "calculette.css". Ecrivons un peu de code question d'avoir un exemple visuel avec lequel travailler !!!

    body{
        padding: 25px;
    }
    button{
        border-radius: 6px;
    }
    .calculette button {
        padding: 5px 10px;
    }
    .calculette > div{
        margin-top: 5px;
    }
    #inputNumber{
        text-align: right;
    }

Bon ok... un petit coup de "F5" et (roulement de tamboure) ... rien D:
Ceci est normale car nous n'avons toujours pas indique a notre page index.html qu'elle possede un CSS. Un peu de code dans notre page web :

    <html>
    <head>
        <link rel="stylesheet" type="text/css" href="calculette.css">
    </head>
    <body>
    [...]

De plus, encapsulons tout nos bouttons dans une nouvelle <div> comme ceci :

    [...]
    <div>
        <input type="text" id="inputNumber" disabled />
    </div>
    <div class="calculette">
        <div>
            <button type="button" id="btnPlus">+</button>
    [...]
        <div>
            <button type="button" class="number">0</button>
        </div>
    </div>

On recommence et OURRAAAA !!!

Maintenant que tout le monde leve la main, je vais expliquer la semantique du CSS avant de repondre au question.

Pour commencer, nous avons deja etablie que l'utiliter du CSS est de styliser et colorer nos elements html. Il est donc normal que nous y retrouvions nos elements avec leur "style". Un exemple :

    body{
        padding: 25px;
    }

Notre appliquons a notre element <body> le "style" du nom "padding" (marge interne) de "25px" (25 pixels, un pixel etant l'uniter de mesure d'un ecran).
En resumer notre element <body>, donc la page entiere car <body> encapsule TOUJOURS tous autres elements HTML, possede maintenant une marge interne de 25 pixels de tous les cotes !

#### 3.2. Structure et organisation ####

>
> De tous les cotes ?!?
>

Effectivement, "padding" peut s'ecrire sous plusieurs forme, les 3 plus comunent sont celles-ci :

    padding: [top] [right] [bottom] [left];
    padding: [top-bottom] [left-right];
    padding: [top-right-bottom-left];

Dans notre cas, nous utilisons la dernieres forme. Pour plus d'information pour les curieux

Je sais ce que vous vous demander deja : 

>
> Quel est l'utiliter du "." et "#" ???
>

Le "." indique au CSS que nous appliquons les styles sur une "class" et "#" sur un element portant un "id" specific.

Le mal de tete est passe ? je vous toujours une main lever.

>
> Pourquoi l'element "button" est deriere la class "calculette" ?
>

Tres bonne question jeune padawan. Ceci signifit simplement "tout element button sous la classe calculette est affecter des styles suivants".
Je prend de l'avance sur la question du charactere "<", ce dernier signifit "tout element div directement sous la classe calculette".

Je vais vous apprendre un petit truc a utiliser avec partimonie, vous pouver appliquer des style directement sur un element dans le HTML ! Prenons notre boutton "0" par exemple :

    [...]
        </div>
            <div>
                <button type="button" class="number" style="margin-left: 36px;">0</button>
            </div>
        </div>
    </body>

Un coup de "F5" et voyer la difference. Maintenant je veux que vous compreniez ceci une chose importante. Le but des CSS est de pouvoir repeter un "style" sur plusieurs "elements" en ecrivant le moins possible. Donc donner un style propre via HTML a un element, vient donc briser ce concepte mais rend aussi votre "style" plus difficile a modifier dans le future. Bien que cette utilisation des style soit extremement puissante et pratique, elle devrais toujours avoir une raison pour appuyer son utilisation. (TL;DR: A utiliser dans des cas particuliers.)

Pour plus d'information et une utilisation plus avancer du CSS je vous recommande de lire le tutoriel de [w3schools.com](https://www.w3schools.com/css/default.asp)

#### 3.3. Framework CSS ####

Vous aurez compris qu'une page web aussi simple semble t-elle possede plusieurs centaines de lignes de style CSS et personne ne veut commencer a partir de zero a chaque site web, personne. C'est exactement pour cela que des librairies CSS existe pour premacher cette tache fastidieuse. En voici quelque exemple :

1. [Bootstrap](http://getbootstrap.com/) par twitter
2. [Materialize](http://materializecss.com/)
3. [Pure CSS](https://purecss.io/) par Yahoo!

Actuellement, le grand leader est [Bootstrap](http://getbootstrap.com/) et plus de la moitier des page web developper actuellement sur le marcher utilise ce Framework CSS.

### 4. Javascript ###

Le javascript est un langage de programmation web executer sur lors d'action effectuer sur votre page web. C'est le code de votre page !!! De la vrai programmation !!! Afin de garder ce tutoriel le plus simple possible, je n'entrerai pas dans les detail dans ce chapitre. Juste assez pour avoir de plaisir.

#### 4.1 Ma premiere ligne de code ####

Afin d'ecrire votre premiere ligne de code, creez un nouveau fichier dans votre repertoire "ma_premiere_page_web" nomme "calculette.js". Ouvrez ce fichier et ecrivez ceci :

    alert("Bonjour a toi humain !");

Vous vous rappelez le probleme rencontrer avec notre fichier CSS ? Meme chose ici, par contre l'ajout de notre fichier est un peu different du CSS :

    [...]
        <div>

        <script type="text/javascript" src="calculette.js"></script>
        
    </body>

Un coup de "ctr + F5" et wow, la page web me repond !

OK, je devine deja les deux mots que vous avez en tete :

>
> Quesseques sspace ???
>

Nous venons d'utiliser la fonction "alert" et lui avons donner comme parametre une chaine de charactere "'Bonjour a toi humain !'". La fonction en elle meme affiche la petite fenetre contenant le message et offre de ce fermer via le boutton quel contient. Tout ceci en une seul fonction ! Merci Javascript, merci.

A tout les courageux qui ne ce sont pas encore sauvez, je vous felicite. Vous venez d'ecrire votre premiere ligne de code !

Mais pourquoi est-ce que la fonction c'est declencher ? Simplement parce qu'en indiquant dans a notre page HTML l'existence du fichier "calculette.js" le navigateur execute sont contenu, notre fonction.

Nous n'avons plus besoin de cette fonction criarde a partir de maintenant, mais il serais dommage de supprimer votre toute premiere ligne de code. Nous allons la "commenter" avec "//" juste devant. Commenter sert a plusieur chose, expliquer un segment de code compliquer ou encore indiquer de ne pas effectuer certaines lignes.

Je dois tout de meme faire un apparte : J'ai mal fait les chose ici. Premierement j'ai code l'integriter du javascript sur le Ballmer peak X| et je n'ai pas toucher a la documentation du cours depuis des mois et je 
sens que ceci se reflettera dans ce qui suit. J'en suis vraiment navre.

#### 4.2. Evenementiation ####
[Missing]

#### 4.3. Ma calculette ####
[Missing]

#### 4.4. Librairie Javascript ####
[Missing]
