// alert("Bonjour a toi Humain !");

var formulaHolder = document.getElementById("formulaHolder");
var input = document.getElementById("inputNumber");
var opperator = null;
var lastInputValue = null;
var isSolve = false;

document.querySelectorAll(".opperator").forEach(function(elem){
    elem.onclick = btnOpperatorClick;
});

document.querySelectorAll(".number").forEach(function(elem) {
    elem.onclick = btnNumberClick;
}); 

function btnNumberClick(){
    if(isSolve == true){
        isSolve = false;
        input.value = "";
    }
    input.value += this.innerHTML;
};

function btnOpperatorClick(){
    var actualOpperator = this.id.split("_")[1];

    if(lastInputValue == null){
        if(opperator == "Plus" || opperator == "Minus"){
            lastInputValue = 0;
        } else if(opperator == "Time"){
            lastInputValue = 1;
        }
    }
    
    if(lastInputValue != null){
        formulaHolder.innerHTML = lastInputValue;
    }

    switch(opperator){
        case "Plus":
            formulaHolder.innerHTML += " + " + input.value;
            lastInputValue += parseFloat(input.value);
            break;
        case "Minus":
            formulaHolder.innerHTML += " - " + input.value;
            lastInputValue -= parseFloat(input.value);
            break;
        case "Time":
            formulaHolder.innerHTML += " * " + input.value;
            lastInputValue *= parseFloat(input.value);
            break;
        case "Per":
            if(lastInputValue == null){
                formulaHolder.innerHTML = input.value + " / 1";
                lastInputValue = parseFloat(input.value);
            } else {
                formulaHolder.innerHTML += " / " + input.value;
                lastInputValue /= parseFloat(input.value);
            }
            break;
    }

    input.value = lastInputValue;
    isSolve = true;
}